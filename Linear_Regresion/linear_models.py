# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 11:38:38 2021

@author: William David Prada Buitrago.
"""
import pickle5 as pickle
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.cross_decomposition import PLSRegression


class PreData():

    """
    This class prepare data to apply linear regresion.
    """

    def __init__(self, df_base):
        """
        This method stores the input DataFrames and checks that they are not empty'.
        Parameters
        ----------
        df_base:
            type: DataFrame
            Information of campaign.
            This Pandas DataFrame must have the same columns of Base_proyecto.csv.
        """

        self.df_base = df_base
    def prepare_data(self):
        """
        This method prepare data to apply any linear regresion model.
        """
        try:
            df_base = self.df_base
            df_columns = list(df_base.columns)
            for n_col in df_columns:
                if len(set(df_base[n_col])) <= 16 and len(set(df_base[n_col])) > 2:
                    cat_var = pd.get_dummies(df_base[n_col], drop_first=True, prefix=n_col)
                    df_base = pd.concat([df_base, cat_var], axis=1)
                    df_base = df_base.drop(columns=[n_col])

            df_base = df_base.drop(columns=['id',
                                  'ad_size',
                                  'hour_of_day_utc',
                                  'app_bundle',
                                  'domain'])

            features = list(df_base.columns)
            features.remove('ctr')
            matrix_x = df_base.loc[:, features].values
            vector_y = df_base.loc[:,['ctr']].values
            return matrix_x, vector_y
        except:
            matrix_x = []
            vector_y = []
            return matrix_x, vector_y

class LinRegression():
    """
    This class apply PCR model or PLS model.
    """
    def __init__(self, matrix_x, vector_y):
        """
        This method stores the input DataFrames and checks that they are not empty'.
        Parameters
        ----------
        matrix_x:
            type: Array
            Information of data training or data to predict.
            This Numpy array must first pass through the PreData class.
        vector_y:
            type: Array
            Result information of data training or data to predict.
            This Numpy array must first pass through the PreData class.
        """
        try:
            self.matrix_x = matrix_x
            self.vector_y = vector_y
        except:
            self.matrix_x = []
            self.vector_y = []
    def train_pcr(self, n_com=2):
        """
        This method train PCR model'.
        Parameters
        ----------
        n_com:
            type: Int
            Number of components to PCA.
            This number must be less than number of columns in matrix_x.
        """
        try:
            matrix_x = self.matrix_x
            vector_y = self.vector_y
            x_train, x_test, y_train, y_test = train_test_split(matrix_x, vector_y, test_size=0.2,
                                                                random_state=1)

            pcr = make_pipeline(StandardScaler(), PCA(n_components=n_com), LinearRegression())
            pcr.fit(x_train, y_train)

            print(f"PCR r-squared with {n_com} components {pcr.score(x_test, y_test):.3f}")
            filename = 'models/pcr_model.sav'
            pickle.dump(pcr, open(filename, 'wb'))
        except:
            print('Model did not change')
    def train_pls(self, n_com=2):
        """
        This method train PLS model'.
        Parameters
        ----------
        n_com:
            type: Int
            Number of components to PLS.
            This number must be less than number of columns in matrix_x.
        """
        try:
            matrix_x = self.matrix_x
            vector_y = self.vector_y
            x_train, x_test, y_train, y_test = train_test_split(matrix_x, vector_y, test_size=0.2,
                                                                    random_state=1)

            pls = PLSRegression(n_components=n_com)
            pls.fit(x_train, y_train)
            print(f"PLS r-squared with {n_com} components {pls.score(x_test, y_test):.3f}")
            filename = 'models/pls_model.sav'
            pickle.dump(pls, open(filename, 'wb'))
        except:
            print('Model did not change')
    def predict(self, matrix_x, model='pls'):
        """
        This method predict new data'.
        Parameters
        ----------
        matrix_x:
            type: Array
            Information of data training or data to predict.
            This Numpy array must first pass through the PreData class.
        model:
            type: string
            Name of linear regresion model.
            This string must have the values "pls" or "pcr".
        """
        try:
            filename = 'models/'+model+'_model.sav'
            loaded_model = pickle.load(open(filename, 'rb'))
            y_pred = loaded_model.predict(matrix_x)
            return y_pred
        except:
            y_pred = []
            return y_pred
