# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 21:55:53 2021

@author: William David Prada Buitrago
"""
#===========================================================================
#EN CASO DE USAR WINDOWS USAR ESTA CELDA
import os
os.getcwd() # Check current directory's path
os.chdir('D:/Linear_Regresion/') # Navigate
#===========================================================================
import pandas as pd
import linear_models as lm

def main():
    n = 27
    df = pd.read_csv("Base_proyecto.csv")
    X, Y = lm.PreData(df).prepare_data()
    lm_1 = lm.LinRegression(matrix_x=X,vector_y=Y)
    lm_1.train_pcr(n_com=n)
    lm_1.train_pls(n_com=n)
    Y_pls = lm_1.predict(matrix_x=X, model='pls')
    Y_pcr = lm_1.predict(matrix_x=X, model='pcr')
    print(f'y with pls model {Y_pls}')
    print(f'y with pcr model {Y_pcr}')
if __name__ == '__main__':
    main()