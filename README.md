# Regresion_multivariable



## Instalacion

1. Cree un entorno en el cual vaya a trabajar con este repositorio
2. Clone el repositorio en la ubicación que desee con el siguiente comando "git clone https://gitlab.com/wpradab/regresion_multivariable.git"
3. Instale los requerimientos con el comando "pip install -r requirements.txt"
4. Guarde la base de datos en la ubicacion "Linear_Regresion/"
5. Pruebe el script funcionamiento_linear_models
